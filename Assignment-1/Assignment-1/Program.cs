﻿
using System;

namespace Assignment_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Warrior w = new Warrior("Wario");
            w.LevelUp(1);
            Weapon axe = new("Axe of Doom", 2, WeaponType.AXE, new(5.0, 1.2));
            w.Equip(axe);
            Console.WriteLine(w.ToString());
            Console.WriteLine("Character equips armor");
            Armor a = new("Boots", 2, ArmorType.PLATE, ItemSlot.LEGS, new() { Strength = 5, Vitality = 10 } );
            w.Equip(a);
            Console.WriteLine(w.ToString());
        }
    }
}
