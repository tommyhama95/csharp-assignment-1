﻿using System;

namespace Assignment_1
{
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// Used to throw Exception when equipping higher level item or wrong item type for 
        /// the Character
        /// </summary>
        public InvalidArmorException () { }
    }
}
