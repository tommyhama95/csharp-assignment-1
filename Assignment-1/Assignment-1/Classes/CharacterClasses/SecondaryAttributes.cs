﻿using System;

namespace Assignment_1
{
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        
        /// <summary>
        /// Based on the Characters' Strength and Dexterity values
        /// </summary>
        public int ArmorRating { get; set; }
        
        /// <summary>
        /// Based on Characters Intelligence value
        /// </summary>
        public int ElementalResistance { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SecondaryAttributes() { }

        /// <summary>
        /// Re-calculates the secondary attributes each time it is constructed
        /// </summary>
        /// <param name="primary">Contains the attributes to set secondary attributes</param>
        public SecondaryAttributes(PrimaryAttributes primary)
        {
            Health = primary.Vitality * 10;
            ArmorRating = primary.Strength + primary.Dexterity;
            ElementalResistance = primary.Intelligence;
        }

        public override bool Equals(object obj)
        {
            return obj is SecondaryAttributes attributes &&
                   Health == attributes.Health &&
                   ArmorRating == attributes.ArmorRating &&
                   ElementalResistance == attributes.ElementalResistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }
    }
}