﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Rouge : Character
    {
        /// <summary>
        /// Contains enum list of allowed weapon for Rouge Class
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes => new() { WeaponType.DAGGER, WeaponType.SWORD };

        /// <summary>
        /// Contains enum list of allowed armor for Rouge Class
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes => new() { ArmorType.LEATHER, ArmorType.MAIL };

        /// <summary>
        /// Sets the Primary attribute that is used for attack calculation.
        /// </summary>
        public override double PrimaryAttackAttribute => Primary.Dexterity;

        /// <summary>
        /// Base constructor for a Rouge Character that passes on the default Character specific Attributes.
        /// Calls CalculateTotalAttributes method which also assignes Secondary Attributes values.
        /// </summary>
        /// <param name="name">Name of Rouge</param>
        public Rouge(string name) : base(name)
        {
            Primary = new PrimaryAttributes();
            Primary.Vitality = 8;
            Primary.Dexterity = 6;
            Primary.Strength = 2;
            CalculateTotalAttributes();
        }

        /// <summary>
        /// Override method to level Rouge with its Character specific attribute values per level.
        /// </summary>
        /// <param name="amount">Amount of levels the Rouge is raised</param>
        public override void LevelUp(int amount)
        {
            ValidateLevelUp(amount);
            Level += amount;
            Primary.Vitality += (3 * amount);
            Primary.Strength += (1 * amount);
            Primary.Dexterity += (4 * amount);
            Primary.Intelligence += (1 * amount);
            CalculateTotalAttributes();
        }
    }
}
