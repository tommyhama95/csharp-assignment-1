﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Mage : Character
    {
        /// <summary>
        /// Contains enum list of allowed Weapon for Mage Class
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes => new() { WeaponType.STAFF, WeaponType.WAND };

        /// <summary>
        /// Contains enum list of allowed armor for Mage Class
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes => new() { ArmorType.CLOTH };

        /// <summary>
        /// Sets the Primary attribute that is used for attack calculation.
        /// </summary>
        public override double PrimaryAttackAttribute => Primary.Intelligence;

        /// <summary>
        /// Base constructor for a Mage Character that passes on the default Character specific Attributes.
        /// Calls CalculateTotalAttributes method which also assignes Secondary Attributes values.
        /// </summary>
        /// <param name="name">Name of Mage</param>
        public Mage(string name) : base(name)
        {
            Primary = new PrimaryAttributes();
            Primary.Intelligence = 8;
            Primary.Vitality = 5;
            CalculateTotalAttributes();
        }

        /// <summary>
        /// Override method to level Mage with its Character specific attribute values per level.
        /// </summary>
        /// <param name="amount">Amount of levels the Mage is raised</param>
        public override void LevelUp(int amount)
        {
            ValidateLevelUp(amount);
            Level += amount;
            Primary.Vitality += (3 * amount);
            Primary.Intelligence += (5 * amount);
            Primary.Strength += (1 * amount);
            Primary.Dexterity += (1 * amount);
            CalculateTotalAttributes();
        }

    }


}

