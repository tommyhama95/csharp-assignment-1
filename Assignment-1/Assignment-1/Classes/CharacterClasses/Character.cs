﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Assignment_1
{
    public abstract class Character
    {

        public string Name { get; set; }
        public int Level { get; set; }

        /// <summary>
        /// Property set by Character specific class to be used for attack value calculation.
        /// </summary>
        public abstract double PrimaryAttackAttribute { get; }
       
        /// <summary>
        /// Property AllowedWeaponTypes is set to abstract to make all child classes override the value for what type of
        /// weapons a Character can wield. Ex Warrior can hold sword, hammer and an axe.
        /// Uses WeaponType enum to specify weapon type: Ex WeaponType.HAMMER.
        /// </summary>
        public abstract List<WeaponType> AllowedWeaponTypes { get; }

        /// <summary>
        /// Property AllowedArmorTYpes is set to abstract to make all child classes override the value of what type of
        /// armor a Character can wield. Ex Warrior can hold plates and mail. 
        /// Uses ArmorType enum to specify armor type: Ex ArmorType.PLATE.
        /// </summary>
        public abstract List<ArmorType> AllowedArmorTypes { get; }

        /// <summary>
        /// Equipment is a Dictionary that keeps track of what Itemslot is in use and to override that value when 
        /// a new equipment is being euipped in that slot. Item can be Armor or Weapon Class. 
        /// Creates new instance on Character creation.
        /// </summary>
        public Dictionary<ItemSlot, Item> Equipment { get; set; } = new();

        /// <summary>
        /// Property to link the Character with its Primary attributes (Ex: Intelligence, Stregnth, etc)
        /// </summary>
        public PrimaryAttributes Primary { get; set; }

        /// <summary>
        /// Property to link the Character with its Secondary attributes that is based on TotalAttributes calculation.
        /// </summary>
        public SecondaryAttributes Secondary { get; set; }

        /// <summary>
        /// Totalattributes used for summarize the Primary and armor attributes values. Used to calculate Secondary attributes.
        /// </summary>
        public PrimaryAttributes TotalAttributes { get; set; }
        

        /// <summary>
        /// Character class that is parent for the child classes (Warrior, Rouge, Ranger and Mage).
        /// All child classes inherits the properties and specificly initiates;
        /// - Character level i set to default of 1.
        /// - PrimaryAttributes of the Character is set to default values.
        /// - SecondaryAttributes is set to default values based on the default contructor of PrimaryAttributes.
        /// </summary>
        /// <param name="name">Name of the Character created</param>
        public Character(string name)
        {
            Name = name;
            Level = 1;
            Primary = new PrimaryAttributes();
            Secondary = new SecondaryAttributes(Primary);
        }

        /// <summary>
        /// Method used to validate if a character can use the input amount to level up. If it is
        /// negative or 0, method will throw an ArgumentException.
        /// </summary>
        /// <param name="amount">Int of the amount a character levels up. Must be above 0</param>
        /// <returns>Return true to approve levelup amount</returns>
        public bool ValidateLevelUp(int amount) 
        {
            if (amount < 1)
                throw new ArgumentException();
            return true;
        }

        /// <summary>
        /// Method to be overriden in child classes to alter the Primary attributes according to their specific character
        /// </summary>
        /// <param name="amount">Takes in the amount of levels a Character should level up</param>
        public abstract void LevelUp(int amount);

        /// <summary>
        /// Method to calculate the attack damage a Character deals. Damage is calculated on its Weapons' Damage and AttackPerSecond values
        /// and using the Character specific PrimaryAttackAttribute. If no weapon the amount will only be multyplied by 1 so the raising factor comes
        /// to the Characters level (which raises PrimaryAttributes) and Attributes effect from Armor items.
        /// </summary>
        /// <returns>Returns a double that is the finished calculation for the Characters' attack value.</returns>
        public double Attack() 
        {
            double damage = 1;
            double attackPS = 1;
            if (Equipment.ContainsKey(ItemSlot.WEAPON))
            {
                Weapon weapon = (Weapon)Equipment[ItemSlot.WEAPON];
                damage = weapon.WeaponAttributes.Damage;
                attackPS = weapon.WeaponAttributes.AttackPerSecond;
            }

            double dps = (damage * attackPS) * (1.0 + (PrimaryAttackAttribute / 100));
            return dps;
        }


        /// <summary>
        /// Method overload to call when passed on Weapon Class. 
        /// Checks if the weapon to be equipped is equiptable by this Character specific Class through the AllowedWeaponTypes List.
        /// If the Character is unable to equipt the weapon throw a custom InvalidWeaponException. If said Character is allowed to
        /// equipt this Weapon, then proceed to add it to the Equipment Dictionary property.
        /// </summary>
        /// <param name="weapon">Weapon Class containing information about the item to be equipped</param>
        /// <returns>Message that confirms if the Weapon has been equipt to the Characters' Equipment property</returns>
        public string Equip(Weapon weapon)
        {
            if (!AllowedWeaponTypes.Contains(weapon.WeaponType) || Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException();
            }
            Equipment[ItemSlot.WEAPON] = weapon;
            return "New weapon equipped!";
        }

        /// <summary>
        /// Same method overload for Equip but instead is only called when a Armor Class is passed. 
        /// Checks if the Armor Class has correct type and level according to Character AllowedArmorType List and leve is higher than Armor.
        /// If true Equip the Armor otherwise throw a custom InvalidArmorException.
        /// If Armor can be equipt, re-calculate and update the Secondary and Primary Attributes through CalculateTotalAttributes method.
        /// </summary>
        /// <param name="armor">Armor Class containing information about the item to be equipped</param>
        /// <returns>Message that confirms if the Armor has been equipt to the Characters' Equipment property</returns>
        public string Equip(Armor armor)
        {
            if (!AllowedArmorTypes.Contains(armor.ArmorType) || Level < armor.RequiredLevel) {
                throw new InvalidArmorException();
            }
            Equipment[armor.Slot] = armor;
            CalculateTotalAttributes();
            return "New armor equipped!";
        }

        /// <summary>
        /// Method iterates through the Characters' Equipment to get Armor attributes that raises the Characters' TotalAttributes together with 
        /// the Characters' PrimaryAttributes. If condition ignores if the Item in the Equipment is the weapon slot.
        /// Creates new PrimaryAttributes with all values set to 0, iteration will add totalt atttributes values through armor together with
        /// Class specific attributes. When done calculating, overwrite TotalAttributes property and Secondary property.
        /// </summary>
        public void CalculateTotalAttributes()
        {
            PrimaryAttributes total = new() { Vitality = 0, Strength = 0 , Dexterity = 0, Intelligence = 0};
            foreach (KeyValuePair<ItemSlot, Item> item in Equipment)
            {
                if(item.Key != ItemSlot.WEAPON && item.Key != ItemSlot.NOTDEFINED)
                {
                    Armor armor = (Armor)item.Value;
                    total = total + armor.Attributes;
                }
            }
            TotalAttributes = total + Primary;
            Secondary = new(TotalAttributes);
        }

        public override string ToString()
        {
            return "=================================\n" +
                $"This {this.GetType().Name}'s name is {Name}\n" +
                $"Level: {Level}\n" +
                $"{Name}'s stats are:\n" +
                $"~ Vitality: {TotalAttributes.Vitality}\n" +
                $"~ Strength: {TotalAttributes.Strength}\n" +
                $"~ Dexterity: {TotalAttributes.Dexterity}\n" +
                $"~ Intelligence: {TotalAttributes.Intelligence}\n" +
                $"  -Health: {Secondary.Health}\n" +
                $"  -Armor Rating: {Secondary.ArmorRating}\n" +
                $"  -Elemental Resistance: {Secondary.ElementalResistance}\n" +
                $"Attack Damage: {this.Attack()}";
        }
    }
}