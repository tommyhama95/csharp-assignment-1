﻿using System;
using System.Collections.Generic;

namespace Assignment_1
{
    public class Warrior : Character
    {
        /// <summary>
        /// Contains enum list of allowed weapons for Warrior Class
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes { get => new() { WeaponType.SWORD, WeaponType.HAMMER, WeaponType.AXE }; }


        /// <summary>
        /// Contains enum list of allowed armor for Warrior Class
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes { get => new() { ArmorType.MAIL, ArmorType.PLATE }; }

        /// <summary>
        /// Sets the Primary attribute that is used for attack calculation.
        /// </summary>
        public override double PrimaryAttackAttribute => TotalAttributes.Strength;

        /// <summary>
        /// Base constructor for a Warrior Character that passes on the default Character specific Attributes.
        /// Calls CalculateTotalAttributes method which also assignes Secondary Attributes values.
        /// </summary>
        /// <param name="name">Name of Warrior</param>
        public Warrior(string name) : base(name)
        {
            Primary = new PrimaryAttributes();
            Primary.Vitality = 10;
            Primary.Strength = 5;
            Primary.Dexterity = 2;
            CalculateTotalAttributes();
        }

        /// <summary>
        /// Override method to level Warrior with its Character specific attribute values per level.
        /// </summary>
        /// <param name="amount">Amount of levels the Warrior is raised</param>
        public override void LevelUp(int amount)
        {
            bool canLevelUp = ValidateLevelUp(amount);
            if(canLevelUp)
            {
                Level += amount;
                Primary.Vitality += (5 * amount);
                Primary.Strength += (3 * amount);
                Primary.Dexterity += (2 * amount);
                Primary.Intelligence += (1 * amount);
                CalculateTotalAttributes();
            }
        }

    }
}
