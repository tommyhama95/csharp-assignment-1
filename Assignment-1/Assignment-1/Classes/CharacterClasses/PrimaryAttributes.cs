﻿using System;

namespace Assignment_1
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        /// <summary>
        /// Default constructor sets all vaules to 1
        /// </summary>
        public PrimaryAttributes()
        {
            Strength = 1;
            Dexterity = 1;
            Intelligence = 1;
            Vitality = 1;
        }

        /// <summary>
        /// Overload operator used to concatenate the attributes of Primary and armor to calculate total attribute values.
        /// </summary>
        /// <param name="basic">Contains the PrimaryAttributes property of the Character Class</param>
        /// <param name="armor">Contains the PrimaryAttributes of the Armor specific attributes the Character has equipped</param>
        /// <returns>Returns the two parameters concatinated as a new PrimaryAttribute</returns>
        public static PrimaryAttributes operator + (PrimaryAttributes basic, PrimaryAttributes armor)
        {
            return new PrimaryAttributes()
            {
                Vitality = basic.Vitality + armor.Vitality,
                Strength = basic.Strength + armor.Strength,
                Dexterity = basic.Dexterity + armor.Dexterity,
                Intelligence = basic.Intelligence + armor.Intelligence
            };
        }

        /// <summary>
        /// Overried method used in testing to compare this class' values with another
        /// </summary>
        /// <param name="obj">Primary Class passed in to compare</param>
        /// <returns>Return true or false depending if values are same or unlike</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }
    }
}