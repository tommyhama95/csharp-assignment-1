﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Ranger : Character
    {
        /// <summary>
        /// Contains enum list of allowed weapon for Ranger Class
        /// </summary>
        public override List<WeaponType> AllowedWeaponTypes => throw new NotImplementedException();

        /// <summary>
        /// Contains enum list of allowed armor for Ranger Class
        /// </summary>
        public override List<ArmorType> AllowedArmorTypes => throw new NotImplementedException();

        /// <summary>
        /// Sets the Primary attribute that is used for attack calculation.
        /// </summary>
        public override double PrimaryAttackAttribute => Primary.Dexterity;

        /// <summary>
        /// Base constructor for a Ranger Character that passes on the default Character specific Attributes.
        /// Calls CalculateTotalAttributes method which also assignes Secondary Attributes values.
        /// </summary>
        /// <param name="name">Name of Ranger</param>
        public Ranger(string name) : base(name)
        {
            Primary = new PrimaryAttributes();
            Primary.Vitality = 8;
            Primary.Dexterity = 7;
            CalculateTotalAttributes();
        }

        /// <summary>
        /// Override method to level Ranger with its Character specific attribute values per level.
        /// </summary>
        /// <param name="amount">Amount of levels the Ranger is raised</param>
        public override void LevelUp(int amount)
        {
            ValidateLevelUp(amount);
            Level += amount;
            Primary.Vitality += (2 * amount);
            Primary.Strength += (1 * amount);
            Primary.Dexterity += (5 * amount);
            Primary.Intelligence += (1 * amount);
            CalculateTotalAttributes();
        }

    }
}
