﻿using System;
using System.Collections.Generic;
using Assignment_1;
using Assignment_1.Classes;

namespace Assignment_1
{
    public class Weapon : Item
    {
        /// <summary>
        /// Enum containing what type of Weapon is created
        /// </summary>
        public WeaponType WeaponType { get; set; }

        /// <summary>
        /// Weapon specific attributes that contain Damage and AttackPerSecond values
        /// </summary>
        public WeaponAttributes WeaponAttributes { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Weapon() : base() { }

        /// <summary>
        /// Override constructor using parameters to customize the weapon
        /// </summary>
        /// <param name="name">Name of Weapon</param>
        /// <param name="requiredLevel">Required level the Character must have to equip</param>
        /// <param name="weaponType">Enum containing what type the Weapon is</param>
        /// <param name="weaponAttributes">Attributes for Weapon boosting attack for the Character</param> 
        public Weapon(
            string name, 
            int requiredLevel, 
            WeaponType weaponType, 
            WeaponAttributes weaponAttributes
            ) 
            : base(name, requiredLevel, ItemSlot.WEAPON)
        {
            WeaponType = weaponType;
            WeaponAttributes = weaponAttributes;
        }

    }
}
