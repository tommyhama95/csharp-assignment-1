﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public abstract class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }

        /// <summary>
        /// Enum to specify which slot the Item takes in Equipment
        /// </summary>
        public ItemSlot Slot { get; set; }

        /// <summary>
        /// Base constructor if no Item properties are specified
        /// </summary>
        public Item()
        {
            Name = "Unknown Item";
            RequiredLevel = 1;
            Slot = ItemSlot.NOTDEFINED;
        }

        /// <summary> with Item specific values
        /// Override constructor
        /// </summary>
        /// <param name="name">Name of Item</param>
        /// <param name="requiredLvl">Required level to use Item</param>
        /// <param name="slot">Which Itemslot the Item takes in the Equipment</param>
        public Item(string name, int requiredLvl, ItemSlot slot)
        {
            Name = name;
            RequiredLevel = requiredLvl;
            Slot = slot;
        }
    }
}
