﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Classes
{
    public class WeaponAttributes
    {
        public double Damage { get; set; }
        public double AttackPerSecond { get; set; }

        /// <summary>
        /// Attributes used to specify Weapon effect for character, used for attack value calculation.
        /// </summary>
        /// <param name="damage">The damage value</param>
        /// <param name="attackPS">Amount of attacks per second</param>
        public WeaponAttributes(double damage, double attackPS)
        {
            Damage = damage;
            AttackPerSecond = attackPS;
        }
    }
}
