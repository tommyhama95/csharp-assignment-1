﻿using System;

namespace Assignment_1
{
    public class Armor : Item
    {
        /// <summary>
        /// Enum containing what type of Armor this item is.
        /// </summary>
        public ArmorType ArmorType { get; set; }

        /// <summary>
        /// Contains the PrimaryAttributes values the Armor gives when equipped
        /// </summary>
        public PrimaryAttributes Attributes { get; set; }

        /// <summary>
        /// Base constructor
        /// </summary>
        public Armor() : base() { }

        /// <summary>
        /// Constructor overrider with parameter values and setting property values
        /// </summary>
        /// <param name="name">Name of the Armor</param>
        /// <param name="requiredLvl">Required level the Character must have to euip said Armor</param>
        /// <param name="armorType">Enum of the Armor type</param>
        /// <param name="slot">Enum of the slot the Armor is made for</param>
        /// <param name="armorAttributes">Attribute values to boost the Character</param>
        public Armor(
            string name, 
            int requiredLvl, 
            ArmorType armorType, 
            ItemSlot slot,
            PrimaryAttributes armorAttributes
            
            ) : base(name, requiredLvl, slot)
        {
            ArmorType = armorType;
            Attributes = armorAttributes;
        }

    }
}
