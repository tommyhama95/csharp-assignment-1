﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    /// <summary>
    /// Enum used to specify what weapon type is constructed
    /// </summary>
    public enum WeaponType
    {
        AXE,
        SWORD,
        HAMMER,
        STAFF,
        WAND,
        DAGGER,
        BOW
    }
}
