﻿using System;

namespace Assignment_1
{
    /// <summary>
    /// Enum used to specify what armor is being initiated
    /// </summary>
    public enum ArmorType
    {
        CLOTH,
        LEATHER,
        PLATE,
        MAIL
    }
}
