﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    /// <summary>
    /// Enum used for specifying which itemslot item is assigned
    /// </summary>
    public enum ItemSlot
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON,
        NOTDEFINED
    }
}
