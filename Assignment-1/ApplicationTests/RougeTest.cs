﻿using System;
using Xunit;
using Assignment_1;

namespace AssignmentTests
{
    public class RougeTest
    {
        [Fact]
        public void ConstructBasic_Rouge_CustomeHeroBasicAttributes()
        {
            PrimaryAttributes expected = new() { Vitality = 8, Dexterity = 6, Strength = 2 };
            Rouge actual = new Rouge("Sneaky S. Nitch");

            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructAndLevelUp_Rouge_AttributesShouldIncrease()
        {
            PrimaryAttributes expected = new() { Vitality = 11, Strength = 3, Dexterity = 10, Intelligence = 2 };
            Rouge actual = new Rouge("Sneaky Meaky");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructLevelUp_Rouge_SecondariesShouldIncrease()
        {
            SecondaryAttributes expected = new() { Health = 110, ArmorRating = 13, ElementalResistance = 2 };
            Rouge actual = new Rouge("Robin");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Secondary);
        }
    }
}
