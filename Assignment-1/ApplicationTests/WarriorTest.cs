﻿using System;
using Xunit;
using Assignment_1;

namespace AssignmentTests
{
    public class WarriorTest
    {
        [Fact]
        public void ConstructBasic_Warrior_CustomHeroBasicAttributes()
        {
            PrimaryAttributes expected = new() { Vitality = 10, Dexterity = 2, Strength = 5, Intelligence = 1 };
            Warrior actual = new Warrior("Kristian");
            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructAndLevelUp_Warrior_AttributesShouldIncrease()
        {
            PrimaryAttributes expected = new() { Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2 };
            Warrior actual = new Warrior("Thor");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructLevelUp_Warrior_SecondariesShouldIncrease()
        {
            SecondaryAttributes expected = new() { Health = 150, ArmorRating = 12, ElementalResistance = 2 };
            Warrior actual = new Warrior("Ree");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Secondary);
        }
    }
}
