﻿using System;
using Xunit;
using Assignment_1;

namespace AssignmentTests
{
   public class MageTest
    {
        [Fact]
        public void Construct_Mage_WithCustomName()
        {
            string expected = "Olaf";
            Mage actual = new Mage("Olaf");

            Assert.Equal(expected, actual.Name);
        }

        [Fact]
        public void ConstructBasic_Mage_CustomHeroBasicAttributes()
        {
            PrimaryAttributes expected = new() { Intelligence = 8, Vitality = 5 };
            Mage actual = new Mage("Olaf");

            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructAndLevelUp_Mage_AttributesShouldIncrease()
        {
            PrimaryAttributes expected = new() { Vitality = 8, Strength = 2, Dexterity = 2, Intelligence = 13};
            Mage actual = new Mage("Olaf");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructLevelUp_Mage_SecondariesShouldIncrease()
        {
            SecondaryAttributes expected = new() { Health = 80, ArmorRating = 4, ElementalResistance = 13 };
            Mage actual = new Mage("Meh");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Secondary);
        }
    }
}
