﻿using System;
using Xunit;
using Assignment_1;

namespace AssignmentTests
{
    public class ArmorTest
    {

        [Fact]
        public void EquipHighLevelArmor_Warrior_ShouldThrowException()
        {
            Warrior warrior = new Warrior("Tina the Destruction of kitties");
            string name = "Plates of Furry Kings";
            int requiredLevel = 100;
            ArmorType armorType = ArmorType.PLATE;
            Armor plates = new Armor(name, requiredLevel, armorType, ItemSlot.BODY, new() { Strength = 16 });

            Assert.Throws<InvalidArmorException>(() => warrior.Equip(plates));
        }
        
        [Fact]
        public void EquipWrongArmor_Warrior_ShouldThrowException()
        {
            Warrior warrior = new Warrior("Mad Max");
            string name = "Cloth of pinkyness";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.CLOTH;
            ItemSlot slot = ItemSlot.BODY;
            Armor cloth = new Armor(name, requiredLevel, armorType, slot, new() { Vitality = 5 });

            Assert.Throws<InvalidArmorException>(() => warrior.Equip(cloth));
        }

        [Fact]
        public void EquipValidArmor_Warrior_ShouldReturnSuccessMessage()
        {
            Warrior warrior = new Warrior("Link");
            string name = "Leg Plates of mystery";
            int requiredLevel = 1;
            ArmorType armorType = ArmorType.PLATE;
            ItemSlot slot = ItemSlot.LEGS;
            Armor plates = new Armor(name, requiredLevel, armorType, slot, new() { Vitality = 3 });

            string expected = "New armor equipped!";
            string actual = warrior.Equip(plates);

            Assert.Equal(expected, actual);
        }
    }
}
