﻿using System;
using Xunit;
using Assignment_1;
using System.Collections.Generic;

namespace AssignmentTests
{
    public class WeaponTest
    {
        [Fact]
        public void EquipHighLevelWeapon_Warrior_ShouldThrowException()
        {
            Warrior warrior = new Warrior("Angry Jon");
            string name = "Mooo-Axe";
            int requiredLevel = 2;
            WeaponType weaponType = WeaponType.AXE;
            Weapon axe = new Weapon(name, requiredLevel, weaponType, new(2, 1));

            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(axe));
        }

        [Fact]
        public void EquipWrongWeaponType_Warrior_ShouldThrowException()
        {
            Warrior warrior = new Warrior("Pretty Betty");
            string name = "Staff of Shining";
            int requiredLevel = 1;
            WeaponType weaponType = WeaponType.STAFF;
            Weapon staff = new Weapon(name, requiredLevel, weaponType, new(1,2));

            Assert.Throws<InvalidWeaponException>(() => warrior.Equip(staff));
        }

        [Fact]
        public void EquipValidWeaponType_Warrior_ShouldReturnSuccessMessage()
        {
            Warrior warrior = new Warrior("Nameless Ben");
            string name = "Wooden Hammer";
            int requiredLevel = 1;
            WeaponType weaponType = WeaponType.HAMMER;
            Weapon hammer = new Weapon(name, requiredLevel, weaponType, new(5, 5));

            string expected = "New weapon equipped!";
            string actual = warrior.Equip(hammer);

            Assert.Equal(expected, actual);
        }
    }
}
