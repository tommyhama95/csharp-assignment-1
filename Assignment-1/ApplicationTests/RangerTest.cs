﻿using System;
using Xunit;
using Assignment_1;

namespace AssignmentTests
{
    public class RangerTest
    {
        [Fact]
        public void ConstructBasic_Ranger_CustomeHeroBasicAttributes()
        {
            PrimaryAttributes expected = new() { Vitality = 8, Dexterity = 7 };
            Ranger actual = new Ranger("Murphy");

            Assert.Equal(expected, actual.Primary);
        }

        [Fact] 
        public void ConstructAndLevelUp_Ranger_AttributesShouldIncrease()
        {
            PrimaryAttributes expected = new() { Vitality = 10, Strength = 2, Dexterity = 12, Intelligence = 2 };
            Ranger actual = new Ranger("Murphy");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Primary);
        }

        [Fact]
        public void ConstructLevelUp_Ranger_SecondariesShouldIncrease()
        {
            SecondaryAttributes expected = new() { Health = 100, ArmorRating = 14, ElementalResistance = 2 };
            Ranger actual = new Ranger("Murphy");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Secondary);
        }
    }
}
