using System;
using Xunit;
using Assignment_1;

namespace AssignmentTests
{
    public class CharacterTest
    {

        [Fact]
        public void FirstLevel_MageExample_ShouldBe1()
        {
            int expected = 1;
            Mage actual = new Mage("Olaf");

            Assert.Equal(expected, actual.Level);
        }

        [Fact]
        public void LevelUp_MageExample_ShouldBe2()
        {
            int expected = 2;
            Mage actual = new Mage("Olaf");
            actual.LevelUp(1);

            Assert.Equal(expected, actual.Level);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_Mage_ShouldThrowArgumentException(int value)
        {
            Mage actual = new Mage("Olaf");

            Assert.Throws<ArgumentException>(() => actual.LevelUp(value));
        }

        /// DPS tests
        
        [Fact] 
        public void DPSNoWeaponEquipped_Warrior_ShouldReturnPrimaryAttackValue()
        {
            Warrior warrior = new Warrior("Ice Tea Man");
            double expected = 1.0 * (1.0 + (5.0 / 100)); // 1.05
            double actual = warrior.Attack();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DPSValidWeapon_Warrior_ShouldReturnDPSWithWeaponIncluded()
        {
            Warrior warrior = new Warrior("Ice Tea Man V.2");
            Weapon axe = new Weapon
            {
                Name = "Axessss",
                RequiredLevel = 1,
                WeaponType = WeaponType.AXE,
                WeaponAttributes = new(7.0, 1.1)
            };
            warrior.Equip(axe);

            double expected = (7.0 * 1.1) * (1.0 + (5.0 / 100)); // 8.085
            double actual = warrior.Attack();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DPSValidWeaponAndArmor_Warrior_ShouldReturnDPSWithBothIncluded()
        {
            Warrior warrior = new Warrior("ULTIMATE ICE TEA MAN");
            Weapon axe = new Weapon
            {
                Name = "Ice axe",
                RequiredLevel = 1,
                WeaponType = WeaponType.AXE,
                WeaponAttributes = new(7.0, 1.1)
            };
            Armor plates = new Armor
            {
                Name = "Tea bags",
                RequiredLevel = 1,
                ArmorType = ArmorType.PLATE,
                Slot = ItemSlot.HEAD,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            warrior.Equip(axe);
            warrior.Equip(plates);

            double expected = (7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100)); //8.162
            double actual = warrior.Attack();

            Assert.Equal(expected, actual);
        }
    }
}
